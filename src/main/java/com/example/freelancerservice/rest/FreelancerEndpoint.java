package com.example.freelancerservice.rest;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.example.freelancerservice.exception.FreelancerNotFound;
import com.example.freelancerservice.model.Freelancer;
import com.example.freelancerservice.model.FreelancerResponse;
import com.example.freelancerservice.service.FreelancerService;
import com.example.freelancerservice.utils.AppConfig;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Path("/")
@Component
public class FreelancerEndpoint {
    private static final Logger logger = LoggerFactory.getLogger(FreelancerEndpoint.class);
    @Autowired
    private FreelancerService freelancerService;

    @Autowired
    private AppConfig config;

    @Autowired
    private FreelancerResponse response;

    // @Value("#{systemProperties['delay.duration']}")
    // private String delayDuration;

    @Value("#{systemProperties['return.error']}")
    private String errorCode;


    private void init() {
        response.setFreelancers(new ArrayList<Freelancer>());
        response.setMessage("");
        response.setSuccess(true);
        response.setVersion(config.getVersion());
    }

    @GET
    @Path("/")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAll() {
        logger.info("List all freelancer");
        logger.info("errorCode:"+errorCode);
        init();
        if(errorCode!=null){
            return Response.status(Response.Status.SERVICE_UNAVAILABLE).build();
        }
        response.setFreelancers(freelancerService.listAll());
        return Response.ok(response)
					 .status(Response.Status.OK).build();
    }

    @GET
    @Path("/{freelancerId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getById(
                            @PathParam("freelancerId") Long freelancerId
                            
                            ) {
        logger.info("Query for Freelancer ID:" + freelancerId);
        init();
        if(errorCode!=null)
            return Response.status(Response.Status.SERVICE_UNAVAILABLE).build();
        try {
            List<Freelancer> list = new ArrayList<Freelancer>();
            list.add(freelancerService.getById(freelancerId));
            response.setFreelancers(list);
            return Response.ok(response)
					 .status(Response.Status.OK).build();
        } catch (FreelancerNotFound e) {
            response.setMessage("FreelancerNotFound");
            response.setSuccess(false);
            return Response.status(Response.Status.NOT_FOUND).
				 entity(response).build();
        }
    }

    @PUT
    @Path("/{freelancerId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response putById(
                            @PathParam("freelancerId") Long freelancerId
                            
                            ) {
        logger.info("PUT for Freelancer ID:" + freelancerId);
        init();
        return Response.status(Response.Status.OK).
				 entity(response).build();
    }

    @POST
    @Path("/{freelancerId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response postById(
                            @PathParam("freelancerId") Long freelancerId
                            
                            ) {
        logger.info("POST for Freelancer ID:" + freelancerId);
        init();
        return Response.status(Response.Status.OK).
				 entity(response).build();
    }

    @DELETE
    @Path("/{freelancerId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteById(
                            @PathParam("freelancerId") Long freelancerId
                            
                            ) {
        logger.info("DELETE for Freelancer ID:" + freelancerId);
        init();
        try {
            freelancerService.getById(freelancerId).getId();
            freelancerService.deleteById(freelancerId);
            return Response.status(Response.Status.OK).
                     entity(response).build();
		} catch (FreelancerNotFound e) {
            response.setSuccess(false);
			return Response.status(Response.Status.NOT_FOUND).
                     entity(response).build();
		}

       
    }
    
    // private static void delayRequest(String delayMs){
    //     long start = System.currentTimeMillis();
    //     logger.info("Checking for delay duration");
    //     if(delayMs != null){
    //         logger.info("Set delay duration time to "+delayMs+" ms");
    //         try {
    //             Thread.sleep(Integer.parseInt(delayMs));
    //             logger.info("Sleep time in ms = "+(System.currentTimeMillis()-start));
    //         } catch (InterruptedException e) {
    //            logger.error(e.getMessage());
    //         }
    //     }else{
    //         logger.info("Delay Duration is not set");
    //     }
       
    // }
    
}