package com.example.freelancerservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan
public class FreelancerServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(FreelancerServiceApplication.class, args);
	}

}
