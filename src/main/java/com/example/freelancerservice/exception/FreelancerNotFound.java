package com.example.freelancerservice.exception;
public class FreelancerNotFound extends Exception {

    private static final long serialVersionUID = -2859369292344754966L;
    @Override
	public String getMessage() {
		return "Freelancer ID not found in database";
	}
    
}