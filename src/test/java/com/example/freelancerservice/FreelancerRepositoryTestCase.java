package com.example.freelancerservice;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import static org.junit.Assert.assertTrue;

import java.util.List;
import java.util.Optional;

import com.example.freelancerservice.model.Freelancer;
import com.example.freelancerservice.service.FreelancerRepository;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("dev")
//@TestPropertySource(locations = "classpath:application-dev.properties")
public class FreelancerRepositoryTestCase {
    @Autowired
    private FreelancerRepository repo;

    @Test
    public void findByIdAndFound(){
        Optional<Freelancer>  freelancers = repo.findById(123456L);
        //List<com.example.freelancerservice.model.Freelancer> freelancers = repo.findById(123456L);
        assertTrue(freelancers.isPresent());
        Freelancer freelancer = freelancers.get();
        assertEquals("Jack", freelancer.getFirstName());
        assertEquals("Teller", freelancer.getLastName());
        assertEquals("jteller@sons.com", freelancer.getEmail());

    }

    @Test
    public void findByIdAndNotFound(){
        Optional<Freelancer>  freelancers= repo.findById(1L);
        assertTrue(!freelancers.isPresent());

    }

    @Test
    public void listAll(){
        List<com.example.freelancerservice.model.Freelancer> freelancers = repo.findAll();
        assertNotNull(freelancers);
    }

    
}