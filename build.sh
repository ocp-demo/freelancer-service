#!/bin/sh
PROJECT=$1
APP_NAME=freelancer-service
APP_VERSION=$2
OPENJDK_IS=redhat-openjdk18-openshift
OPENJDK_TAG=1.5
APP_BINARY_PATH=target/$APP_NAME-$APP_VERSION.jar
oc get bc $APP_NAME -n $PROJECT
if [ $? -ne 0 ];
then
oc new-build --binary=true \
--name=$APP_NAME \
--image-stream=$OPENJDK_IS:$OPENJDK_TAG \
-n $PROJECT
fi
# mvn clean package
oc start-build $APP_NAME \
--from-file=$APP_BINARY_PATH  \
--follow -n $PROJECT
oc tag $APP_NAME:latest \
$APP_NAME:$APP_VERSION -n $PROJECT
oc new-app --image-stream=$APP_NAME:$APP_VERSION --name=$APP_NAME --labels app=$APP_NAME,version=$APP_VERSION
# oc new-app freelancer-service:1.0.1 \
# --name=freelancer-v2 --labels=freelancer-service=false,app=freelancer-service \
# SUBTITLE="shrad B" COLOR="blue"
