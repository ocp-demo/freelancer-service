INSERT INTO freelancer (id, firstname, lastname,email) VALUES (123456,'Jack', 'Teller','jteller@sons.com');
INSERT INTO freelancer (id, firstname, lastname,email) VALUES (234567,'Clay', 'Morrow','cmorrow@sons.com');
INSERT INTO freelancer (id, firstname, lastname,email) VALUES (345678,'Chibs', 'Telford','ctelford@sons.com');
INSERT INTO freelancer (id, firstname, lastname,email) VALUES (456789,'Jemma', 'Morrow','jmorrow@sons.com');
INSERT INTO freelancer_skills (id, skill, freelancer_id,detail) VALUES (1,'java',123456,'Java EE development');
INSERT INTO freelancer_skills (id, skill, freelancer_id,detail) VALUES (2,'javaScript',123456,'Node.js and Angular'); 
INSERT INTO freelancer_skills (id, skill, freelancer_id,detail) VALUES (3,'vert.x',234567,'Microservice');
INSERT INTO freelancer_skills (id, skill, freelancer_id,detail) VALUES (4,'java',234567,'Java EE Certified');
INSERT INTO freelancer_skills (id, skill, freelancer_id,detail) VALUES (5,'go',345678,'Specialist in rewrite SpringBoot to Go');