package com.example.freelancerservice.service;

import java.util.List;
import java.util.Optional;

import com.example.freelancerservice.model.Freelancer;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface FreelancerRepository extends JpaRepository<Freelancer, Long> {
    Optional<Freelancer> findById(Long id);
    List<Freelancer> findAll();
    void deleteById(Long id);
}