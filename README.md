# Freelancer Service

Freelancer Service is RESTful API application for query freelancers information from RDBMS

 Freelancer Service use following main technology
- Spring Boot
- JAX-RS, JPA
- JDBC H2
- Rest Assure
- Fabric8
- ConfigMap

## Test with JUnit

This service used JUnit, Rest Assure for testing by using H2 database on memory as RDBMS

```
mvn test
```

## Running with Local Profile

This service can be run locally on your machine by following command

```
java -Dspring.profiles.active=dev -jar target/freelancer-service-1.0.0-SNAPSHOT.jar
```

Properties file for local profile located at src/main/java/resources/application-dev.properties


## Deploy to OpenShift
Create configmap by using shell scipt
```
etc/create_configmap.sh
```

Fabric8 plugin can be used to deploy to OpenShfit with S2I by
```
mvn fabric8:deploy -Popenshift
```

Following screen shot shows data on H2

![GitHub Logo](/images/freelancer-data.png)
### API document

Following URI are supported

| Method | URI                                   |  Description                |
|--------|---------------------------------------|-----------------------------
| GET    | /freelancers                  |  get all freelancers        |
| GET    | /freelancers/{freelancerId}   |  get freelancer by ID       | 
| DELETE | /freelancers/{freelancerId}   |  delee freelancer by ID       | 


Following data can be used for test when run locally
- Freelancer ID: 123456,234567,345678,456789

Remark: if you build with OpenShift, you can use mirror maven repository by

```bash
MAVEN_MIRROR_URL = http://nexus.ci-cd.svc.cluster.local:8081/repository/maven-all-public/
```

## Authors

* **Voravit** 
