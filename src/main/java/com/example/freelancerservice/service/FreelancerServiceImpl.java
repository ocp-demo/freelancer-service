package com.example.freelancerservice.service;

import java.util.List;

import com.example.freelancerservice.exception.FreelancerNotFound;
import com.example.freelancerservice.model.Freelancer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import java.util.Optional;

@Component
public class FreelancerServiceImpl implements FreelancerService {

    private static final Logger logger = LoggerFactory.getLogger(FreelancerServiceImpl.class);
    
    @Autowired
    FreelancerRepository freelancerRepo;

    @Override
    public Freelancer getById(Long freelancerId) throws FreelancerNotFound{
        logger.info("Query ID:"+freelancerId);
        Optional<Freelancer> freelancers = freelancerRepo.findById(freelancerId);
        if(freelancers.isPresent()){
            Freelancer freelance = freelancers.get();
            logger.info("Found freelancer ID: "+freelance.getId());
            return freelance;
        }else{
            throw new FreelancerNotFound();
        }
        
    }

    @Override
    public List<Freelancer> listAll() {
        List<Freelancer> freelancers = freelancerRepo.findAll();
        return freelancers;
    }

	@Override
	public void deleteById(Long freelancerId) {
		freelancerRepo.deleteById(freelancerId);
    }
}